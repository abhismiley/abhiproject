package com.bby.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = "com.bby.*")

public class BigQueryApp {

	public static void main(String[] args) {
		SpringApplication.run(BigQueryApp.class, args);
	}

}
