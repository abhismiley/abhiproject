package com.bby.authorization;

import java.io.IOException;
import java.util.List;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.bigquery.Bigquery;
import com.google.api.services.bigquery.BigqueryScopes;
import com.google.api.services.bigquery.model.GetQueryResultsResponse;
import com.google.api.services.bigquery.model.QueryRequest;
import com.google.api.services.bigquery.model.QueryResponse;
import com.google.api.services.bigquery.model.TableRow;


public class BigQueryAuthorization {

	public Bigquery createAuthorizedClient() throws IOException {
		HttpTransport transport = new NetHttpTransport();
	    JsonFactory jsonFactory = new JacksonFactory();
	    GoogleCredential credential = GoogleCredential.getApplicationDefault(transport, jsonFactory);

	    if (credential.createScopedRequired()) {
	      credential = credential.createScoped(BigqueryScopes.all());
	    }

	    return new Bigquery.Builder(transport, jsonFactory, credential)
	        .setApplicationName("Bigquery Samples")
	        .build();
	}
    
	  /**
	   * Executes the given query synchronously.
	   */
	public  List<TableRow> executeQuery(String querySql, Bigquery bigquery, String projectId)
	      throws IOException {
	    QueryResponse query =
	        bigquery.jobs().query(projectId, new QueryRequest().setQuery(querySql)).execute();

	    // Execute it
	    GetQueryResultsResponse queryResult =
	        bigquery
	            .jobs()
	            .getQueryResults(
	                query.getJobReference().getProjectId(), query.getJobReference().getJobId())
	            .execute();

	    return queryResult.getRows();
	  }

}
